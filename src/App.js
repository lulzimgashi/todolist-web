import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import SockJs from 'sockjs-client';
import Stomp from 'stompjs';
import { connect } from 'react-redux';

//components
import Start from './screens/Start';
import Home from './screens/Home';
import Loading from './components/Loading';

class App extends Component {

  componentDidUpdate() {
    if (this.props.user && !this.connected) {
      //this.connect();
    }
  }

  render() {
    return (
      <BrowserRouter>
        <div style={{ width: '100%', height: '100%' }}>
          {this.props.loading && <Loading />}
          {!this.props.user ? <Start /> :
            <Switch>
              <Route path="/" component={Home} />
            </Switch>}
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = state => {
  return { loading: state.loading, user: state.user }
}

export default connect(mapStateToProps)(App);
