import React from 'react';
import ReactDOM from 'react-dom';
import './assets/index.css';
import './assets/loader.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import reducers from './redux-stuff/reducers';

const middleware = [thunk]

export const store = createStore(
    reducers,
    applyMiddleware(...middleware)
)


ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>, document.getElementById('root'));
registerServiceWorker();
