import { combineReducers } from 'redux';
import user from './user';
import toDoList from './toDoList';
import loading from './loading';

const rootReducer = combineReducers({
    user,toDoList,loading
});

export default rootReducer;