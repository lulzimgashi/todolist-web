import { LOGIN_DONE, LOGOUT_DONE } from '../actions/types';

export default (state = null, action) => {
    switch (action.type) {
        case LOGIN_DONE:
            return action.payload;
        case LOGOUT_DONE:
            return null;
        default:
            return state;
    }
}