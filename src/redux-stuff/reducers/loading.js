import { LOADING_TRUE, LOADING_FALSE, LOGIN_DONE, ADD_TO_DO, REMOVE_TO_DO, UPDATE_TO_DO, LOGOUT_DONE } from '../actions/types';

export default (state = false, action) => {
    switch (action.type) {
        case LOADING_TRUE:
            return true;
        case LOADING_FALSE:
            return false;
        case LOGIN_DONE:
            return false;
        case LOGOUT_DONE:
            return false;
        case ADD_TO_DO:
            return false;
        case REMOVE_TO_DO:
            return false;
        case UPDATE_TO_DO:
            return false;
        default:
            return state;

    }
}