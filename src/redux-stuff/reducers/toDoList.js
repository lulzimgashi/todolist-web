import { LOGIN_DONE, REMOVE_TO_DO, ADD_TO_DO, UPDATE_TO_DO } from '../actions/types';

export default (state = { 0: [], 1: [], 2: [] }, action) => {
    switch (action.type) {
        case LOGIN_DONE:
            const user = action.payload;
            var toDoList = { 0: [], 1: [], 2: [] };

            for (var e in user.toDoList) {
                if (user.toDoList[e].status === 0) {
                    toDoList[0].push(user.toDoList[e]);
                }
                if (user.toDoList[e].status === 1) {
                    toDoList[1].push(user.toDoList[e]);
                }
                if (user.toDoList[e].status === 2) {
                    toDoList[2].push(user.toDoList[e]);
                }
            }
            return toDoList;
        case REMOVE_TO_DO:
            var id = action.payload.id;
            var listId = action.payload.listId;
            var index = null;

            for (var e in state[listId]) {
                if (state[listId][e].id === id) {
                    index = e;
                    break;
                }
            }

            state[listId].splice(index, 1);
            return { ...state };
        case ADD_TO_DO:
            var toDo = action.payload.toDo;

            state[toDo.status].push(toDo);

            return { ...state };
        case UPDATE_TO_DO:
            var status = action.payload.status;
            var last = 0;
            if (status >= 1) {
                last = status - 1;
            } else {
                last = 2;
            }

            state[status].push(action.payload);

            var index = null;
            for (var e in state[last]) {
                if (state[last][e].id === id) {
                    index = e;
                    break;
                }
            }

            state[last].splice(0,1);
            return {...state};
        default:
            return state;
    }
}
