import { ADD_TO_DO, LOADING_TRUE, REMOVE_TO_DO, LOADING_FALSE, UPDATE_TO_DO, LOGOUT_DONE } from './types';
import { API } from '../../configuration'
import axios from 'axios';

export const addToDo = (toDo, user) => dispatch => {
    dispatch({ type: LOADING_TRUE });

    var payload = {
        listId: toDo.status,
        toDo
    }

    axios.post(API + 'to-dos', toDo, {
        auth: {
            username: user.email,
            password: user.password
        }
    }).then(response => {
        payload.toDo = response.data;
        dispatch({ type: ADD_TO_DO, payload })
    }).catch(err => {
        if (err.response.status === 401) {
            dispatch({ type: LOGOUT_DONE });
        } else {
            dispatch({ type: LOADING_FALSE });
        }
    })

}

export const updateToDo = (toDo, user, statusChanged) => dispatch => {
      dispatch({ type: LOADING_TRUE });

    axios.put(API + 'to-dos', toDo, {
        auth: {
            username: user.email,
            password: user.password
        }
    }).then(response => {
        if (statusChanged) {
            dispatch({ type: UPDATE_TO_DO, payload: response.data });
        } else {
            dispatch({ type: LOADING_FALSE });
        }
    }).catch(err => {
        if (err.response.status === 401) {
            dispatch({ type: LOGOUT_DONE });
        } else {
            dispatch({ type: LOADING_FALSE });
        }
    })
}


export const deleteToDo = (toDo, user) => dispatch => {
    dispatch({ type: LOADING_TRUE });

    var payload = {
        id: toDo.id,
        listId: toDo.status
    }

    axios.delete(API + 'to-dos/' + toDo.id, {
        auth: {
            username: user.email,
            password: user.password
        }
    }).then(response => {
        dispatch({ type: REMOVE_TO_DO, payload })
    }).catch(err => {
        if (err.response.status === 401) {
            dispatch({ type: LOGOUT_DONE });
        } else {
            dispatch({ type: LOADING_FALSE });
        }
    });
}