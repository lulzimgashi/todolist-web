import { LOADING_TRUE, LOGIN_DONE, LOGOUT_DONE,LOADING_FALSE } from './types'
import { API } from '../../configuration'
import axios from 'axios';

export const login = user => dispatch => {
    dispatch({ type: LOADING_TRUE });

    axios.get(API + 'users/login', {
        auth: {
            username: user.email,
            password: user.password
        }
    }).then(response => {
        response.data.password=user.password;
        dispatch({ type: LOGIN_DONE, payload: response.data })
    }).catch(err => {
        dispatch({ type: LOADING_FALSE });
        if (err.response.status === 401) {

        } else {
            
        }
    })

}

export const logout = () => dispatch => {
    dispatch({ type: LOGOUT_DONE });
}

export const register = user => dispatch => {

    dispatch({ type: LOADING_TRUE });
    axios.post(API+'users',user)
    .then(response=>{
        response.data.password=user.password;
        dispatch({ type: LOGIN_DONE, payload: response.data })
    }).catch(err=>{
        dispatch({ type: LOADING_FALSE });
    })
}