import React, { Component } from 'react';
import { connect } from 'react-redux';

//Screen
import LoginTab from './LoginTab';
import RegisterTab from './RegisterTab';

class Start extends Component {
    state = { tab: 0 }

    changeTab = (tab) => {
        if (this.state.tab !== tab) {
            this.setState({ tab: tab });
        }
    }

    render() {
        return (
            <div className='start-tabs'>
                <div className='start-tab-header'>
                    <div onClick={() => this.changeTab(0)} className={'start-tab-header-item weight-700 ' + (this.state.tab === 0 ? 'selected' : '')}>LOGIN</div>
                    <div onClick={() => this.changeTab(1)} className={'start-tab-header-item weight-700 ' + (this.state.tab === 1 ? 'selected' : '')}>REGISTER</div>
                </div>
                <div className='start-tab-content' >
                    <LoginTab tab={this.state.tab} changeTab={this.changeTab} />
                    <RegisterTab tab={this.state.tab} changeTab={this.changeTab} />
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {}
}

export default connect(mapStateToProps)(Start);