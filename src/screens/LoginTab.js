import React, { Component } from 'react';
import { connect } from 'react-redux';
//actions
import { login } from '../redux-stuff/actions';


class LoginTab extends Component {

    login = () => {
        if (this.refs.email.value.length > 0 && this.refs.password.value.length > 0) {
            var user= { 
                email:this.refs.email.value,
                password:this.refs.password.value
            }
            this.props.dispatch(login(user));
        }
    }

    render() {
        return (
            <div className='start-tab' style={{ display: this.props.tab === 0 ? 'flex' : 'none' }}>
                <input ref='email' className='start-input' placeholder='Email' />
                <input ref='password' type='password' className='start-input' placeholder='Password' />
                <div onClick={this.login} className='start-button'>LOGIN</div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {}
}

export default connect(mapStateToProps)(LoginTab);