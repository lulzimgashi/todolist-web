import React, { Component } from 'react';
import { connect } from 'react-redux';
//components
import CardItem from '../components/CardItem';
import AddButton from '../components/AddButton';
import AddForm from '../components/AddForm';
import QRCode from 'qrcode.react';
//actions
import { logout } from '../redux-stuff/actions';


class Home extends Component {
    state = { addForm: false, selected: 1 }

    componentWillMount() {
        this.setState({ toDoList: this.props.toDoList, toDoListFiltered: this.props.toDoList[1] });
    }

    componentWillReceiveProps(nextProps) {
        if (this.state.selected === 3) {
            var all = [];
            all.push(...nextProps.toDoList[0])
            all.push(...nextProps.toDoList[1])
            all.push(...nextProps.toDoList[2])
            this.setState({ toDoListFiltered: all });
        } else {
            this.setState({ toDoListFiltered: nextProps.toDoList[this.state.selected] });
        }
    }


    renderRow = () => {
        return this.state.toDoListFiltered.map(toDo => {
            return <CardItem key={toDo.id} item={toDo} />
        })
    }

    filter = (index) => {
        switch (index) {
            case 0:
                this.setState({ toDoListFiltered: this.state.toDoList[0], selected: 0 })
                break;
            case 1:
                this.setState({ toDoListFiltered: this.state.toDoList[1], selected: 1 })
                break;
            case 2:
                this.setState({ toDoListFiltered: this.state.toDoList[2], selected: 2 })
                break;
            case 3:
                var all = [];
                all.push(...this.state.toDoList[0])
                all.push(...this.state.toDoList[1])
                all.push(...this.state.toDoList[2])

                this.setState({ toDoListFiltered: all, selected: 3 });
                break;
        }
    }

    switchForm = (value) => {
        this.setState({ addForm: value });
    }

    logout = () => {
        this.props.dispatch(logout());
    }

    render() {
        const backglogLength = this.state.toDoList[0].length;
        const onProgressLength = this.state.toDoList[1].length;
        const doneLength = this.state.toDoList[2].length;
        const allLength = backglogLength + onProgressLength + doneLength;

        return (
            <div className='home' >
                <AddButton switchForm={this.switchForm} />
                {this.state.addForm && <AddForm switchForm={this.switchForm} />}
                <div className='qr-code-wrapper' >
                    <QRCode size={80} bgColor='white' value={this.props.user.id} />
                </div>

                <div className='home-header' >
                    <div className='home-header-icons-wrapper' >
                        <i onClick={this.logout} className='fas fa-power-off margin-left-10 pointer ' />
                    </div>
                    <h2>To Do List</h2>
                    <div className='home-header-stats-wrapper'>
                        <div onClick={() => this.filter(3)} className={'home-header-stat-item-wrapper ' + (this.state.selected === 3 ? 'selected' : '')} >
                            <span className='home-header-stat-span'>All List</span>
                            <h4 className='home-header-custom-h4'>{allLength}</h4>
                        </div>

                        <div onClick={() => this.filter(0)} className={'home-header-stat-item-wrapper ' + (this.state.selected === 0 ? 'selected' : '')} >
                            <span className='home-header-stat-span'>Backlog</span>
                            <h4 className='home-header-custom-h4'>{backglogLength}</h4>
                        </div>

                        <div onClick={() => this.filter(1)} className={'home-header-stat-item-wrapper ' + (this.state.selected === 1 ? 'selected' : '')} >
                            <span className='home-header-stat-span'>On Progress</span>
                            <h4 className='home-header-custom-h4'>{onProgressLength}</h4>
                        </div>

                        <div onClick={() => this.filter(2)} className={'home-header-stat-item-wrapper ' + (this.state.selected === 2 ? 'selected' : '')} >
                            <span className='home-header-stat-span'>Done</span>
                            <h4 className='home-header-custom-h4'>{doneLength}</h4>
                        </div>
                    </div>
                </div>

                <div className='home-cards-container' >
                    {this.renderRow()}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return { user: state.user, toDoList: state.toDoList };
}

export default connect(mapStateToProps)(Home);