import React, { Component } from 'react';
import { connect } from 'react-redux';
//actions
import { register } from '../redux-stuff/actions';

class RegisterTab extends Component {

    register = () => {
        if (this.refs.firstName.value.length > 0
            && this.refs.lastName.value.length > 0
            && this.refs.email.value.length > 0
            && this.refs.password.value.length > 0) {
                var user={
                    firstName:this.refs.firstName.value,
                    lastName:this.refs.lastName.value,
                    email:this.refs.email.value,
                    password:this.refs.password.value,
                }

                this.props.dispatch(register(user));
        }
    }

    render() {
        return (
            <div className='start-tab' style={{ display: this.props.tab === 1 ? 'flex' : 'none' }}>

                <input ref='firstName' className='start-input' placeholder='First Name' />
                <input ref='lastName' className='start-input' placeholder='Last Name' />
                <input ref='email' className='start-input' placeholder='Email' />
                <input ref='password' type='password' className='start-input' placeholder='Password' />
                <div onClick={this.register} className='start-button'>REGISTER</div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {}
}

export default connect(mapStateToProps)(RegisterTab);