import React, { Component } from 'react';
import { connect } from 'react-redux';
//actions
import { updateToDo, deleteToDo } from '../redux-stuff/actions'

class CardItem extends Component {
    state = { edit: false, item: {} }


    componentWillMount() {
        this.setState({ item: this.props.item });
    }

    changeStatus = () => {
        var status = 0;
        if (!this.state.edit) {
            switch (this.state.item.status) {
                case 0:
                    status = 1;
                    break;
                case 1:
                    status = 2;
                    break;
                case 2:
                    status = 0;
                    break;
            }
        }

        var item = this.state.item;
        item.status = status;

        this.props.dispatch(updateToDo(this.state.item, this.props.user, true));
    }

    enterEditMode = () => {
        this.setState({ edit: true });
    }

    save = () => {

        if (this.refs.input.value.length > 0) {
            var item = this.state.item;
            item.text = this.refs.input.value
            this.props.dispatch(updateToDo(item, this.props.user, false));
            this.setState({ item: item, edit: false });
        } else {
            this.setState({ edit: false })
        }
    }

    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.save();
        }
    }

    deleteToDo = () => {
        this.props.dispatch(deleteToDo(this.state.item, this.props.user));
    }

    render() {

        var status = '';
        switch (this.state.item.status) {
            case 0:
                status = 'BACKLOG';
                break;
            case 1:
                status = 'ON PROGRESS';
                break;
            case 2:
                status = 'DONE';
                break;
        }

        return (
            <div key={this.state.item.id} className='home-card-wrapper'>
                <div className='home-card-header'>
                    {this.state.edit ?
                        <input ref='input' onBlur={this.save} onKeyPress={this._handleKeyPress} autoFocus className='weight-500 border-none em-0-9 margin-0 padding-0 padding-bottom-1px border-bottom-blue' defaultValue={this.state.item.text} /> :
                        <span onClick={this.enterEditMode} className='home-card-name-span'>{this.state.item.text}</span>}
                    <span onClick={this.changeStatus} className='home-card-status-span margin-top-5'>{status}</span>
                </div>
                <div className='home-card-bottom' >
                    <div onClick={this.deleteToDo} className='home-card-button color-red'>Delete</div>
                </div>
            </div>
        );
    }
}

const mapPropsToState = state => {
    return { user: state.user }
}

export default connect(mapPropsToState)(CardItem);