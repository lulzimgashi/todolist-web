import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addToDo } from '../redux-stuff/actions';


class AddForm extends Component {
    state = { status: 1 }

    changeState = (status) => {
        if (this.state.status !== status) {
            this.setState({ status: status });
        }
    }

    hideForm = () => {
        this.props.switchForm(false);
    }

    addToDo = () => {
        if (this.refs.name.value.length > 0) {
            var toDo = {
                text:this.refs.name.value,
                status:this.state.status,
                userId:this.props.user.id
            }

            this.props.dispatch(addToDo(toDo,this.props.user));
            this.hideForm();
        }   
    }

    render() {
        var status = this.state.status;
        return (
            <div className='home-add-form' >
                <input autoFocus ref='name' className='start-input color-white padding-0 margin-0' placeholder='Name' />
                <div className='home-add-from-status-wrapper margin-top-20' >
                    <span onClick={() => this.changeState(0)} className={'pointer margin-bottom-10 ' + (status === 0 ? 'weight-700' : '')}>BACKLOG</span>
                    <span onClick={() => this.changeState(1)} className={'pointer margin-bottom-10 ' + (status === 1 ? 'weight-700' : '')}>ON PROGRESS</span>
                    <span onClick={() => this.changeState(2)} className={'pointer margin-bottom-10 ' + (status === 2 ? 'weight-700' : '')}>DONE</span>
                </div>
                <div onClick={this.hideForm} className='home-add-from-submit-button color-lightgrey margin-top-20'>CANCEL</div>
                <div onClick={this.addToDo} className='home-add-from-submit-button margin-top-10'>SUBMIT</div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {user:state.user}
}

export default connect(mapStateToProps)(AddForm);