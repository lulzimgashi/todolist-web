import React, { Component } from 'react';


class AddButton extends Component {
    showAddForm=()=>{
        this.props.switchForm(true);
    }
    render() {
        return (
            <div onClick={this.showAddForm} className='home-add-button' >
                <i className='fas fa-plus' />
            </div>
        );
    }
}

export default AddButton;