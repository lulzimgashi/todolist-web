import React, { Component } from 'react';
import { connect } from 'react-redux';

function mapStateToProps(state) {
    return {

    };
}

class Loading extends Component {
    render() {
        return (
            <div className='loader-wrapper' >
                <div className='loader' >
                </div>
            </div>
        );
    }
}

export default connect(mapStateToProps)(Loading);